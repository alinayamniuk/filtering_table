import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { products } from './models/product-data';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { FiltersFormFields, Product } from "./models/table.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  public readonly filterFormFieldsEnum: typeof FiltersFormFields = FiltersFormFields;
  public readonly categories: string[] = ['Vegetables', 'Grocery', 'Fruit'];

  public products: Product[] = products;
  public filteredProducts$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>(this.products);

  public form: FormGroup;

  public productsPerPage: number = 5;
  public currentPage: number = 0;

  constructor(
    private readonly cdRef: ChangeDetectorRef,
    private readonly fb: FormBuilder
  ) {}

  public ngOnInit(): void {
    this.initForm();
    this.applyFilters();
  }

  public initForm(): void {
    this.form = this.fb.group({
      [FiltersFormFields.CATEGORY]: [''],
      [FiltersFormFields.MIN_PRICE]: [null, Validators.min(0)],
      [FiltersFormFields.MAX_PRICE]: [null],
      [FiltersFormFields.STOCK_AMOUNT]: [null, Validators.min(0)],
      [FiltersFormFields.NAME]: ['']
    });

    this.form.valueChanges.subscribe(() => {
      this.applyFilters();
    });
  }

  public get paginatedProducts(): Product[] {
    const startIndex: number = this.currentPage * this.productsPerPage;
    const endIndex: number = startIndex + this.productsPerPage;

    return this.filteredProducts$.value.slice(startIndex, endIndex);
  }

  public onPaginateChange(event: PageEvent): void {
    this.productsPerPage = event.pageSize;
    this.currentPage = event.pageIndex;

    this.cdRef.detectChanges();
  }

  public applyFilters() {
    let { category, minPrice, maxPrice, stockAmount, name } = this.form.value;

    const filteredProducts: Product[] = this.products.filter(product => {
      return (
        this.getCategoryFilter(product, category) &&
        this.getPriceFilter(product, minPrice, maxPrice) &&
        this.getStockAmountFilter(product, stockAmount) &&
        this.getNameFilter(product, name)
      );
    });

    this.filteredProducts$.next(filteredProducts);
    this.currentPage = 0;

    this.onPaginateChange({
      pageSize: this.productsPerPage,
      pageIndex: this.currentPage
    } as PageEvent);
  }

  public getCategoryFilter(product: Product, category: string): boolean {
    return category ? product.category.includes(category.toLowerCase()) : true;
  }

  public getPriceFilter(product: Product, minPrice: number, maxPrice: number): boolean {
    if (!minPrice && !maxPrice) return true;

    const productPrice = parseFloat(product.price.replace('$', ''));

    return (minPrice ? productPrice >= minPrice : true) && (maxPrice ? productPrice <= maxPrice : true);
  }

  public getStockAmountFilter(product: Product, stockAmount: number): boolean {
    return stockAmount ? product.stock_amount >= stockAmount : true;
  }

  public getNameFilter(product: Product, name: string): boolean {
    return name ? product.name.toLowerCase().includes(name.toLowerCase()) : true;
  }

  public isAnyFormControlErrors(formControlName: FiltersFormFields, errorKey: string): boolean {
    return this.form.get(formControlName)?.errors?.[errorKey];
  }

  public trackById(index: number, item: Product): string {
    return item.id;
  }
}
