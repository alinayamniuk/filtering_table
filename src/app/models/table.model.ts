export interface Product {
  id: string;
  name: string;
  stock_amount: number;
  price: string;
  category: string;
}

export enum FiltersFormFields {
  CATEGORY = 'category',
  MIN_PRICE = 'minPrice',
  MAX_PRICE = 'maxPrice',
  STOCK_AMOUNT = 'stockAmount',
  NAME = 'name'
}
